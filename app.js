'use strict'

var SwaggerHapi = require('swagger-hapi')
var Hapi = require('hapi')
var app = new Hapi.Server()
var mongoose = require('mongoose')
const conf = require('config')

module.exports = app // for testing

var config = {
  appRoot: __dirname // required config
}

SwaggerHapi.create(config, function (err, swaggerHapi) {
  if (err) { throw err; }
  var port = process.env.PORT || 10010
  app.connection({ port: port })
  app.address = function () {
    return { port: port }
  }

  app.register(swaggerHapi.plugin, function (err) {
    if (err) { return console.error('Failed to load plugin:', err); }
    mongoose.Promise = Promise

    mongoose.connect(conf.db.url, { useNewUrlParser: true })

    mongoose.connection.on('error', console.error.bind(console, 'connection error:'))

    mongoose.connection.once('open', function () {
      app.start(function () {
        if (swaggerHapi.runner.swagger.paths['/hello']) {
          console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott')
        }
      })
    })
  })
})
