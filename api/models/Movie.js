var mongoose = require('mongoose')
mongoose.Promise = Promise

module.exports = mongoose.model('Movie', new mongoose.Schema({
  title: String,
  director: String
}))
