var util = require("util");
const Movie = require("../models/Movie");

module.exports = {
  get,
  post,
  getOneByID
};

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function get(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  Movie.find(
    { title: { $exists: true }, director: { $exists: true } },
    (err, movies) => {
      if (err) {
        res.status(500).json(err);
        return;
      }

      res.json({
        movies: movies
      });
    }
  );
}

function getOneByID(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  Movie.findOne(
    {
      _id: req.swagger.params.id.value,
      title: { $exists: true },
      director: { $exists: true }
    },
    (err, movie) => {
      if (err) {
        res.status(500).json(err);
        return;
      }

      res.json(movie);
    }
  );
}

function post(req, res) {
  const movie = new Movie(req.swagger.params.movie.value);
  movie.save(err => {
    if (err) {
      res
        .status(500)
        .json(err)
        .end();
      return;
    }

    res.json(movie);
  });
}
